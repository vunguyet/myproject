#include <iostream>
#include <iomanip>
#include <stdio.h>
using namespace std;
class HCN{

float d,r;
public:
    void nhap();
    float s();
    float p();
     void ve();
};
void HCN::nhap(){
cout<<"chieu dai: "; cin>>d;
cout<<"chieu rong: "; cin>>r;
}
float HCN::p(){
return (d+r)*2;
}
float HCN::s(){
return d*r;
}
void HCN::ve(){
    for(int i=0;i<d;i++)
    {
        for(int j=0;j<r;j++)
            cout<<"*";
        cout<<endl;
    }
}
int main()
{
    HCN abc;
    abc.nhap();
    cout<<"dien tich: "<<abc.s()<<endl;
    cout<<"chu vi: "<<abc.p()<<endl;
    abc.ve();
    return 0;
}
